module.exports = {
  testPathIgnorePatterns: [
    '<rootDir>/.next/',
    '<rootDir>/node_modules/',
    '<rootDir>/coverage/'
  ],
  setupFiles: [
    '<rootDir>/tests/setup.js',
  ],
  setupFilesAfterEnv: [
    '<rootDir>/tests/setupAfterEnv.js'
  ],
  testMatch: [
    '**/?(*.)+(spec|test).[jt]s?(x)',
  ],
  transform: {
    "^.+\\.(js|jsx|ts|tsx)$": "<rootDir>/node_modules/babel-jest"
  },
  collectCoverageFrom: [
    '**/*.{js,jsx}',
    '!**/.next/**',
    '!**/node_modules/**',
    '!**/tests/**',
    '!**/coverage/**',
    '!.eslint*',
    '!.prettier*',
    '!*.config.js',
  ],
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: 90,
    },
  },
};
