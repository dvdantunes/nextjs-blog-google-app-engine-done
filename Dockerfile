# Dockerfile
#
# - Using Node LTS Erbium (v12) with Alpine 3.12 as base image
#   @see https://nodejs.org/en/about/releases/
#   @see https://github.com/nodejs/docker-node/blob/de78cda8d04c7c3a5c45302dcce22d5ea258f7ef/12/alpine3.12/Dockerfile
#
# - Using multi-stage builds
#   @see @see https://learnk8s.io/blog/smaller-docker-images


# First stage
FROM node:12.18.4-alpine3.12 as build

LABEL maintainer="David Antunes <dvdantunes@gmail.com>"


# Setting environment vars
ENV APP_HOME /app
ENV APP_USER app


# Create app home directory and set it as current directory
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

# Copy app files
COPY . .

# Install node.js dependencies
RUN yarn --frozen-lockfile --production

# Build production env
RUN yarn build



# Second stage
FROM node:12.18.4-alpine3.12

# Add tini init (pid 1) handler
# @see https://www.elastic.io/nodejs-as-pid-1-under-docker-images/
RUN apk add --no-cache tini

# Setting environment vars
ENV APP_HOME /app
ENV APP_USER app

# Set app home directory
WORKDIR $APP_HOME

# Copy app files
COPY --from=build $APP_HOME .

# Expose node port
EXPOSE 8080

# Set default exec command
CMD ["yarn", "start"]

# Set entrypoint
ENTRYPOINT ["tini", "-gwv", "--"]
