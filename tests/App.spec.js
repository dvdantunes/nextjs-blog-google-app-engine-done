import { render, screen } from "@testing-library/react";
import App from "../pages/index";
import { getAllPosts } from '../lib/api';

describe("BlogApp", () => {

  it("should render app", () => {
    const allPosts = getAllPosts([
      'title',
      'date',
      'slug',
      'author',
      'coverImage',
      'excerpt',
    ]);

    render(<App allPosts={allPosts} />);

    expect(screen.getByRole("heading", { name: "Blog" })).toBeInTheDocument();
  });
});
