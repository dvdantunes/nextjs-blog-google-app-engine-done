module.exports = {
  extends: ['./node_modules/poetic/config/eslint/eslint-config.js'],
  rules: {},
  env: {
    "jest": true
  },
  overrides: [
    {
      files: ['*.js'],
      rules: {
        '@typescript-eslint/no-var-requires': 'off',
        '@typescript-eslint/camelcase': 'off',
        // 'camelcase': 'off',
        // 'no-unused-vars': 'off',
        // 'func-names': 'off',
      },
    },
  ],
};
